package ru.andreymarkelov.atlas.plugin.qcf.workflow.function;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.opensymphony.workflow.loader.AbstractDescriptor;

import java.util.HashMap;
import java.util.Map;

public class AppendWatchersToLinkedIssuesFunctionFactory extends BaseFunctionFactory {
    public AppendWatchersToLinkedIssuesFunctionFactory(
            CustomFieldManager customFieldManager,
            JiraAuthenticationContext jiraAuthenticationContext) {
        super(customFieldManager, jiraAuthenticationContext);
    }

    @Override
    public Map<String, ?> getDescriptorParams(Map<String, Object> functionParams) {
        Map<String, Object> descriptorParams = new HashMap<String, Object>();

        if (functionParams != null && functionParams.containsKey("assignee")) {
            descriptorParams.put("assignee", extractSingleParam(functionParams, "assignee"));
        } else {
            descriptorParams.put("assignee", null);
        }
        if (functionParams != null && functionParams.containsKey("reporter")) {
            descriptorParams.put("reporter", extractSingleParam(functionParams, "reporter"));
        } else {
            descriptorParams.put("reporter", null);
        }
        if (functionParams != null && functionParams.containsKey("multiLinkerField")) {
            descriptorParams.put("multiLinkerField", extractSingleParam(functionParams, "multiLinkerField"));
        } else {
            descriptorParams.put("multiLinkerField", "");
        }

        return descriptorParams;
    }

    @Override
    protected void getVelocityParamsForEdit(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        velocityParams.put("assignee", getParam(descriptor, "assignee"));
        velocityParams.put("reporter", getParam(descriptor, "reporter"));
        velocityParams.put("customFields", getLinkerMultiFields());
        velocityParams.put("multiLinkerField", getParam(descriptor, "multiLinkerField"));
    }

    @Override
    protected void getVelocityParamsForInput(Map<String, Object> velocityParams) {
        velocityParams.put("customFields", getLinkerMultiFields());
    }

    @Override
    protected void getVelocityParamsForView(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        velocityParams.put("assignee", getParam(descriptor, "assignee"));
        velocityParams.put("reporter", getParam(descriptor, "reporter"));
        velocityParams.put("multiLinkerField", getParam(descriptor, "multiLinkerField"));
    }
}

