package ru.andreymarkelov.atlas.plugin.qcf.workflow.function;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;

public class LinkIssueFunction extends AbstractJiraFunctionProvider {
    private final static Logger log = Logger.getLogger(LinkIssueFunction.class);

    private final CustomFieldManager customFieldManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final IssueManager issueManager;
    private final IssueLinkManager issueLinkManager;
    private final IssueLinkTypeManager issueLinkTypeManager;

    public LinkIssueFunction(
            CustomFieldManager customFieldManager,
            JiraAuthenticationContext jiraAuthenticationContext,
            IssueManager issueManager,
            IssueLinkManager issueLinkManager,
            IssueLinkTypeManager issueLinkTypeManager) {
        this.customFieldManager = customFieldManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.issueManager = issueManager;
        this.issueLinkManager = issueLinkManager;
        this.issueLinkTypeManager = issueLinkTypeManager;
    }

    @Override
    public void execute(
            @SuppressWarnings("rawtypes") Map transientVars,
            @SuppressWarnings("rawtypes") Map args,
            PropertySet ps) throws WorkflowException {
        MutableIssue issue = getIssue(transientVars);
        String multiLinkerField = (String) args.get("multiLinkerField");
        String linkTypeId = (String) args.get("linkType");

        CustomField customField = customFieldManager.getCustomFieldObjectByName(multiLinkerField);
        if (customField == null) {
            log.error("Link Issue Function: Multi Linker custom field is not set");
            throw new WorkflowException(jiraAuthenticationContext.getI18nHelper().getText("queryfields.postfunction.linkissues.error"));
        }
        IssueLinkType issueLinkType = issueLinkTypeManager.getIssueLinkType(Long.valueOf(linkTypeId));
        if (issueLinkType == null) {
            log.error("Link Issue Function: link type with id " + linkTypeId + " does not exist.");
            throw new WorkflowException(jiraAuthenticationContext.getI18nHelper().getText("queryfields.postfunction.linkissues.error"));
        }
        List<String> issuesKeys = (List<String>) issue.getCustomFieldValue(customField);
        if(issuesKeys != null){
            addLinksToIssues(getIssuesFromKeys(issuesKeys), issue, issueLinkType);
        }
    }

    private List<Issue> getIssuesFromKeys(List<String> issuesKeys) {
        List<Issue> issues = new ArrayList<Issue>();
        for (String issuesKey : issuesKeys) {
            Issue issue = issueManager.getIssueObject(issuesKey);
            if (issue != null) {
                issues.add(issue);
            }
        }
        return issues;
    }

    	private void addLinksToIssues(
            List<Issue> issues,
            Issue sourceIssue,
            IssueLinkType issueLinkType) throws WorkflowException {
        ApplicationUser user = jiraAuthenticationContext.getLoggedInUser();
        for (Issue issue : issues) {
            addLinkToIssue(sourceIssue.getId(), issue.getId(), issueLinkType.getId(), user);
        }
    }

    private void addLinkToIssue(
            Long sourceIssueId,
            Long destinationIssueId,
            Long issueLinkTypeId,
            ApplicationUser user) throws WorkflowException {
        try {
            issueLinkManager.createIssueLink(sourceIssueId, destinationIssueId, issueLinkTypeId, null, user);
        } catch (CreateException ex) {
            log.error("Link Issue Function: error create issue link");
            throw new WorkflowException(jiraAuthenticationContext.getI18nHelper().getText("queryfields.postfunction.linkissues.error"));
        }
    }
}
