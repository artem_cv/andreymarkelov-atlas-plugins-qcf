package ru.andreymarkelov.atlas.plugin.qcf.workflow.function;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginFunctionFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.FunctionDescriptor;
import org.apache.commons.lang3.StringUtils;
import ru.andreymarkelov.atlas.plugin.qcf.field.LinkerMultiField;

import java.util.ArrayList;
import java.util.List;

abstract class BaseFunctionFactory extends AbstractWorkflowPluginFactory implements WorkflowPluginFunctionFactory {
    protected final CustomFieldManager customFieldManager;
    protected final JiraAuthenticationContext jiraAuthenticationContext;

    public BaseFunctionFactory(
            CustomFieldManager customFieldManager,
            JiraAuthenticationContext jiraAuthenticationContext) {
        this.customFieldManager = customFieldManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    protected List<CustomField> getLinkerMultiFields() {
        List<CustomField> linkerMultiFields = new ArrayList<CustomField>();
        for (CustomField customField : customFieldManager.getCustomFieldObjects()) {
            if (customField.getCustomFieldType() instanceof LinkerMultiField
                    || LinkerMultiField.class.isAssignableFrom(customField.getCustomFieldType().getClass())) {
                linkerMultiFields.add(customField);
            }
        }
        return linkerMultiFields;
    }

    protected String getParam(AbstractDescriptor descriptor, String param) {
        if (!(descriptor instanceof FunctionDescriptor)) {
            throw new IllegalArgumentException("Descriptor must be a FunctionDescriptor.");
        }

        FunctionDescriptor validatorDescriptor = (FunctionDescriptor) descriptor;
        String value = (String) validatorDescriptor.getArgs().get(param);
        return StringUtils.isNotBlank(value) ? value : null;
    }
}
